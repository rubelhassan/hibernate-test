package com.rubel.controller;

import com.rubel.dao.PersonDao;
import com.rubel.model.Person;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

/**
 * Created by rubel on 8/22/17.
 */
@Controller
public class HomeController {

    @Autowired
    PersonDao personDao;

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public ModelAndView home(){
        return new ModelAndView("home");
    }

    @RequestMapping(value = "/person/save/{name}", method = RequestMethod.GET)
    @ResponseBody
    public Person savePerson(@PathVariable(value = "name") String name){
        System.out.println(name + ":name here");
        Person person = personDao.saveOrUpdate(new Person(name));
        return person;
    }

    @RequestMapping(value = "/person/get/{name}", method = RequestMethod.GET)
    @ResponseBody
    public Person getPerson(@PathVariable(value = "name") String name){
        Person person = personDao.find(name);
        return person;
    }

    @RequestMapping(value = "test", method = RequestMethod.GET)
    @ResponseBody
    public Person test(){
        Person person = new Person("Rubel Hassan");
        return person;
    }
}
