package com.rubel.dao;

import com.rubel.model.Person;

import java.util.List;

/**
 * Created by rubel on 8/22/17.
 */
public interface PersonDao {

    List<Person> findAll();
    Person delete(Person user);
    Person saveOrUpdate(Person user);
    Person find (String name);

}
