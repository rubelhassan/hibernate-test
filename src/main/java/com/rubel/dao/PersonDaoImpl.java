package com.rubel.dao;

import com.rubel.model.Person;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;

/**
 * Created by rubel on 8/22/17.
 */
@Repository
@Transactional
public class PersonDaoImpl implements PersonDao {

    @Autowired
    SessionFactory session;

    public List<Person> findAll() {
        return null;
    }

    public Person delete(Person user) {
        return null;
    }

    public Person saveOrUpdate(Person user) {
        Person person = find(user.getName());

        if(person == null)
            session.getCurrentSession().save(user);

        else session.getCurrentSession().update(person);

        return user;
    }

    public Person find(String name) {
        Query<Person> query = session.getCurrentSession()
                .createQuery("from Person p where p.name=:name", Person.class);

        query.setParameter("name", name);

        Person person = query.uniqueResult();

        return person;
    }
}
