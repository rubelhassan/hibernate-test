package com.rubel.model;

/**
 * Created by rubel on 8/22/17.
 */
public class Skill {
    String name;

    public Skill() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
